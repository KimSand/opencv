"""
This module is intended for use while testing the Junit test, for the java part.
"""
from enum import Enum
import socket
import sys
from socketClientLibrary import Client

MAPSTART = "MAPSTART"
MAPEND = "MAPEND"
BALLSSTART = "BALLSSTART"
BALLSEND = "BALLSEND"
POSESTART = "POSESTART"
POSEEND = "POSEEND"
CROSSSTART = "CROSSSTART"
CROSSEND = "CROSSEND"
ABORT = "ABORT"
GOALSTART = "GOALSTART"
GOALEND = "GOALEND"


def send_test_svg(client):
    try:
        print("Started sending mapfile")
        client.send_data(MAPSTART)
        file = open("path.svg", "r")
        for line in file:
            client.send_data(line)
        client.send_data(MAPEND)
        print("Finished sending mapfile!")
    except:
        send_test_svg(client)
        raise Exception
def send_balls_positions(client, ballspositions):
    try:
        print("Started sending balls")
        client.send_data(BALLSSTART)
        if not ballspositions:
            client.send_data("")
        else:
            client.send_data(str(ballspositions['x']) + "," + str(ballspositions['y']))
        client.send_data(BALLSEND)
        print("Finished sending balls")
    except:
        send_balls_positions(client,ballspositions)
        raise Exception



def send_cross_positions(client, crosspos):
    try:
        print("Started sending cross")
        client.send_data(CROSSSTART)
        if crosspos:
            client.send_data(str(crosspos['x']) + "," + str(crosspos['y']))
        else:
            client.send_data("")
        client.send_data(CROSSEND)
        print("Finished sending cross")
    except:
        send_cross_positions(client,crosspos)
        raise Exception


def send_robot_positions(client, robotposition):
    try:
        print("Started sending pose")
        client.send_data(POSESTART)
        if not robotposition:
            client.send_data("")
        else:
            client.send_data(str(robotposition['x']) + "," + str(robotposition['y'])+","+str(robotposition["heading"]))
        client.send_data(POSEEND)
        print("Finished sending pose")
    except:
        send_robot_positions(client,robotposition)
        raise Exception

def send_goal_positions(client, goalpos):
    try:
        print("Started sending cross")
        client.send_data(GOALSTART)
        if not goalpos:
            client.send_data("")
        else:
            client.send_data(str(goalpos['x']) + "," + str(goalpos['y']))
        client.send_data(GOALEND)
        print("Finished sending cross")
    except:
        send_goal_positions(client,goalpos)
        raise Exception



def receive_data(client):
    try:
        receivedData = str(client.receive_data())
        print(f"Raw received data:{receivedData}\t type: {type(receivedData)}")
        if "BALL" in receivedData:
            return "BALL"
        elif "POSE" in receivedData:
            return "POSE"
        elif "None" in receivedData:
            return "NONE"
        else:
            print(f"Error received something i dont understand: {receivedData}")
            return "ERROR"
    except socket.timeout as e:
        #print("TIMEOUT")
        #err = e.args[0]
        #print(err)
        return "NODATA"
    except:
        print("unexpeceted error:",sys.exc_info())
        raise Exception


def test_case():
    """
    This is the test case
    :return: None
    """
    client = Client()
    while True:
        input_int = int(input("Press text case number to run test:"))
        if input_int == 0:
            break
        if input_int == 1:
            client.send_data(ABORT)
            print("Message send!")
        if input_int == 2:

            send_balls_positions(client,{"x":2.2,"y":3.3})
        if input_int == 3:
            print("Sending file")
            send_test_svg(client)
        if input_int == 4:
            print(receive_data(client))

    input("Hit enter to close program...")


if __name__ == "__main__":
    test_case()
