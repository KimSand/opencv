import json as json
import cv2
import math
import numpy as np
import perspective
import socketClient
import random
import time
camheight = 175.6
robotHeight = 39.1 #40.0 #39.20 #38.75
ballHeight = 4.0
borderHeight = 7.0
crossHeight = 3.3
imageWidth = 1280
imageHeight = 720
networkMode = True
resend_positional_stuff = True
crossMoving = False
calibrationMode = True
maxTriesForBall = 3
def nothing(x):
    pass

#Initialize the sliders for the windows and declare camera recording dimensions
def initializeSliders():
    cv2.namedWindow("robot")
    cv2.createTrackbar('thresh-lwr-robot-green', 'robot', 105, 255, nothing)
    cv2.createTrackbar('thresh-upr-robot-green', 'robot', 255, 255, nothing)
    cv2.createTrackbar('param1', 'robot', 1, 255, nothing)
    cv2.createTrackbar('param2', 'robot', 10, 255, nothing)
    cv2.createTrackbar('minRadius', 'robot', 0, 255, nothing)
    cv2.createTrackbar('max', 'robot', 0, 255, nothing)

    cv2.namedWindow('balls')
    cv2.createTrackbar('thresh-lwr-balls', 'balls', 207, 255, nothing)
    cv2.createTrackbar('thresh-upr-balls', 'balls', 255, 255, nothing)

    cv2.createTrackbar('balls-arc-lwr', 'balls', 25, 255, nothing)
    cv2.createTrackbar('balls-arc-upr', 'balls', 100, 255, nothing)
    cv2.namedWindow('map')
    cv2.createTrackbar('pixel-diameter', 'map', 15, 255, nothing)
    cv2.createTrackbar('color-closeness', 'map', 61, 255, nothing)
    cv2.createTrackbar('pixel-closeness', 'map', 0, 255, nothing)
    cv2.createTrackbar('size-uppr-thresh', 'map', 255, 255, nothing)
    cv2.createTrackbar('size-lwr-thresh', 'map', 153, 255, nothing)

    # De her to laver vist ikke noget?
    cv2.createTrackbar('smallest-edge', 'map', 50, 255, nothing)
    cv2.createTrackbar('largest-edge', 'map', 150, 255, nothing)

    cv2.createTrackbar('thresh-cross', 'map', 16, 255, nothing)
    cv2.createTrackbar('min-line-length-cross', 'map', 0, 255, nothing)
    cv2.createTrackbar('max-line-length-cross', 'map', 255, 1000, nothing)

    cv2.createTrackbar('thresh-border', 'map', 140, 255, nothing)
    cv2.createTrackbar('min-line-length-border', 'map', 125, 255, nothing)
    cv2.createTrackbar('max-line-gap-border', 'map', 255, 255, nothing)

    cap.set(cv2.CAP_PROP_FRAME_WIDTH, imageWidth)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, imageHeight)

#Not working...
def calibration():
    ret, frame = cap.read()

    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((7 * 9, 3), np.float32)
    objp[:, :2] = np.mgrid[0:9, 0:7].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, (9, 7), None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(gray, corners, (20, 20), (-1, -1), criteria)
        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(frame, (7, 9), corners2, ret)

        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
        h, w = frame.shape[:2]
        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
        #print(roi)
        mapx, mapy = cv2.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w, h), 5)
        dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
        # dst = cv2.undistort(frame, mtx, dist, None, newcameramtx)
        if roi[0] != 0 and roi[1] != 0 and roi[2] != 0 and roi[3] != 0:
            # crop the image
            # x, y, w, h = roi

            # dst = dst[y:y + h, x:x + w]
            cv2.imwrite('calibresult.png', dst)

        cv2.imshow("img fixed", dst)

        # cv2.waitKey(500)
    cv2.imshow('img', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        return

#Used for calibration phase when drawing points on the screen
def mouse_drawing_field(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDOWN:
        fieldpoints.append(({"x": x, "y": y}))
    if event == cv2.EVENT_MBUTTONDOWN:
        fieldpoints.pop()

#Used to project the objects coordinates
def findCorrectedPosition(objectHeight, objectX, objectY, centerOfCamera):
    if objectX is not None and objectY is not None:
        newX = objectX - centerOfCamera[0]["x"]
        newY = objectY - centerOfCamera[0]["y"]

        h1 = camheight
        h2 = camheight - objectHeight
        factor = h2 / h1

        newX *= factor
        newY *= factor

        newX += centerOfCamera[0]['x']
        newY += centerOfCamera[0]['y']
        return newX, newY
    return objectX, objectY

#Mark origo (center of screen) on the screen
def showMidOfScreen(origo):
    cv2.circle(originalPerspectiveCorrectedFrame, (origo[0]["x"], origo[0]["y"]), 1, (255, 0, 0), 3)

#Calc distance between two points
def calculateDistanceBetweenTwoPoints(x1, x2, y1, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)


def FindPoint(x1, y1, x2,
              y2, x, y) :
    if (x > x1 and x < x2 and
        y > y1 and y < y2) :
        return True
    else :
        return False

#Specify the perimiter in which bals are removed as they are too close to the robot itself
def removeBallsCloseToRobot(ballsposition, smallcircle, bigcircle):
    debug = False
    filteredballs = []
    threshold = abs(calculateDistanceBetweenTwoPoints(smallcircle["x"], bigcircle["x"], smallcircle["y"], bigcircle["y"]) * 2)
    for ball in ballsposition:
        dist = abs(calculateDistanceBetweenTwoPoints(ball["x"], bigcircle["x"], ball["y"], bigcircle["y"]))
        if dist >= threshold:
            filteredballs.append(ball)
        else:
            if debug: ("removed ball")
    if debug:
        print(f"applied threshold:{threshold}")
        print(f"filtered balls{filteredballs}")
        print(f"len filtered{len(filteredballs)}\t len original: {len(ballsposition)}")
    return filteredballs

#Draw an array of balls
def drawBalls(frame, ballspositions, color):
    for ball in ballspositions:
        cv2.putText(frame, "Ball", (round(ball["x"] - 25), round(ball["y"] - 25)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        cv2.circle(frame, (round(ball["x"]), round(ball["y"])), 1, color, -1)
    return frame

#Main method here
def objectdetection(originalWarpedFrame, lines):
    orientationpoints = []  # The two circles on the robot used for positioning
    ballspositions = [] #Contains all the balls we detect
    robotPosition = {}
    crossMid = {}
    showrobotcameraposition = False  # Show the "fake" robot position on gui

    originalGrayFrame = cv2.cvtColor(originalWarpedFrame, cv2.COLOR_BGR2GRAY) #Convert from BGR to Gray scale

    a, edgesDetected, frame = cleanupImage(originalWarpedFrame, originalGrayFrame) # Do some cleanup of the frames

    #cv2.imshow("Original greyscale", originalGrayFrame)
    findRobotCircles(orientationpoints, originalWarpedFrame, showrobotcameraposition) #Find the robot circles and fill it into orientation points
    #print(f"orientationpoints: {orientationpoints}")
    bigcircle, smallcircle, heading = findBigAndSmallCircleAndOrientation(orientationpoints, originalWarpedFrame,
                                                                          showrobotcameraposition) #Find the big and small circle and orientation from the orientation points

    if (bigcircle and smallcircle):
        robotPosition = bigcircle

    showMidOfScreen(findMidOfScreen(originalWarpedFrame))

    detectedBallsFrame, ballspositions = detectBalls(originalGrayFrame)
    linescross = detectCross(edgesDetected, frame)
    crossMid = findCrossMid(crossMid, linescross, originalWarpedFrame)
    maxline, scalingfactor = findScalingFactorAndDrawLines(lines, originalGrayFrame)
    drawCross(linescross, originalWarpedFrame)

    if robotPosition:
        #print(robotPosition)
        if ballspositions:
            #Remove false balls indicators caused by robot shape and colour
            ballspositions = removeBallsCloseToRobot(ballspositions, smallcircle, bigcircle)
            #Remove balls outside inner border
            ballspositions = removeBallsOutsideField(ballspositions)
            #print(f"amount of balls after deletion: {len(ballspositions)}")

            #originalWarpedFrame = drawBalls(originalWarpedFrame, ballspositions,(255,0,0))
            correctBallsPositions(ballspositions, originalWarpedFrame)
            originalWarpedFrame = drawBalls(originalWarpedFrame, ballspositions,(0,255,0))

        cv2.putText(originalWarpedFrame, f"FOUND ROBOT",
                    (25, originalWarpedFrame.shape[0] - 50),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1)
    else:
        originalWarpedFrame = drawBalls(originalWarpedFrame, ballspositions, (255, 0, 0))
        # Remove balls outside inner border
        ballspositions = removeBallsOutsideField(ballspositions)
        #Correct balls for perspective
        correctBallsPositions(ballspositions, originalWarpedFrame)
        originalWarpedFrame = drawBalls(originalWarpedFrame, ballspositions, (0, 255, 0))
        cv2.putText(originalWarpedFrame, f"NOT FOUND ROBOT",
                    (25, originalWarpedFrame.shape[0] - 50),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1)
    # Correct perspective for markers on robot


    cv2.imshow("detectedballsframe", detectedBallsFrame)
    cv2.imshow("final", originalWarpedFrame)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        return



    return lines, linescross, scalingfactor, ballspositions, maxline, robotPosition, heading, crossMid
    cv2.destroyAllWindows()
    cap.release()


def removeBallsOutsideField(ballspositions):
    filteredballs = []
    for ball in ballspositions:
        if FindPoint(innerBorderMinX, innerBorderMinY, innerBorderMaxX, innerBorderMaxY, ball["x"], ball["y"]):
            filteredballs.append(ball)
    ballspositions = filteredballs
    return ballspositions


#Find cross mid and draw it
def findCrossMid(crossMid, linescross, originalWarpedFrame):
    if linescross is not None:
        if linescross.any():
            maxX = linescross[0][0][0]
            maxY = linescross[0][0][1]
            minX = linescross[0][0][0]
            minY = linescross[0][0][1]
            for line in linescross:
                for x1, y1, x2, y2 in line:
                    maxX = max(maxX, x1, x2)
                    maxY = max(maxY, y1, y2)
                    minX = min(minX, x1, x2)
                    minY = min(minY, y1, y2)
            diffX = maxX - minX
            diffY = maxY - minY

            crossMid = {"x": minX + (diffX / 2), "y": minY + (diffY / 2)}

            crossMid["x"], crossMid["y"] = findCorrectedPosition(crossHeight, crossMid["x"], crossMid["y"],
                                                                 findMidOfScreen(originalWarpedFrame))
            cv2.circle(originalWarpedFrame, (int(round(crossMid["x"])), int(round(crossMid["y"]))), 1, (255, 0, 255), 3)

        # print(f"Detected cross mid without correction {crossMid} maxX {maxX}  maxY {maxY}")
    return crossMid


def correctBallsPositions(ballspositions, originalWarpedFrame):
    if(len(ballspositions) != 0):
        #print(ballspositions)
        for ball in ballspositions:
            ball["x"], ball["y"] = findCorrectedPosition(ballHeight, ball["x"], ball["y"],
                                                         findMidOfScreen(originalWarpedFrame))


def drawCross(linescross, originalGrayFrame):
    if linescross is not None:
        for line2 in linescross:
            for x1, y1, x2, y2 in line2:
                cv2.line(originalGrayFrame, (x1, y1), (x2, y2), (0, 255, 0), 1)


    cv2.imshow("cross",originalGrayFrame)

#Find scaling factor for converting from pixels to cm
def findScalingFactorAndDrawLines(boundaryLines, originalGrayFrame):
    longestLineOnField = 166.5 #Longest line on the field is 166.5 cm
    maxline = 0;
    if boundaryLines is not None:
        for line in boundaryLines:
            maxline = max(maxline,
                          abs(math.sqrt(abs((line["x2"] - line["x1"]) ** 2) + abs((line["y2"] - line["y1"]) ** 2))))
            #cv2.line(originalGrayFrame, (line["x1"], line["y1"]), (line["x2"], line["y2"]), (255, 255, 255), 1)
    if maxline is not 0:
        scalingfactor = longestLineOnField / maxline
    else:
        scalingfactor = 1
    return maxline, scalingfactor


def correctRobotPositionPerspective(bigcircle, originalWarpedFrame, smallcircle):
    #print(f"SmallCircle {smallcircle}\t BigCircle {bigcircle}")
    bigcircle["x"], bigcircle["y"] = findCorrectedPosition(robotHeight, bigcircle["x"], bigcircle["y"],
                                                           findMidOfScreen(originalWarpedFrame))
    smallcircle["x"], smallcircle["y"] = findCorrectedPosition(robotHeight, smallcircle["x"], smallcircle["y"],
                                                               findMidOfScreen(originalWarpedFrame))
    cv2.circle(originalWarpedFrame, (round(bigcircle["x"]), round(bigcircle["y"])), 1, (0, 0, 255), 3)
    cv2.circle(originalWarpedFrame, (round(smallcircle["x"]), round(smallcircle["y"])), 1, (0, 0, 255), 3)
    cv2.arrowedLine(originalWarpedFrame, (round(smallcircle["x"]), round(smallcircle["y"])),
                    (round(bigcircle["x"]), round(bigcircle["y"])), (255, 0, 0))


def findMidOfScreen(originalWarpedFrame):
    origo = []
    origo.append({"x": round((originalWarpedFrame.shape[1] / 2)), "y": round((originalWarpedFrame.shape[0] / 2))})
    return origo

#Find the big and small circle and orientation from the orientation points
def findBigAndSmallCircleAndOrientation(orientationpoints, originalWarpedFrame, showrobotcameraposition):
    angle = 0
    heading = 0
    bigcircle = {}
    smallcircle = {}

    if (len(orientationpoints) == 2):
        denominator = orientationpoints[1]["x"] - orientationpoints[0]["x"]
        smallcircle = orientationpoints[0]
        bigcircle = orientationpoints[1]
        if bigcircle["area"] - smallcircle["area"] < 0:
            smallcircle = orientationpoints[1]
            bigcircle = orientationpoints[0]
        #Special edge case if the two circles are exactly above each other
        if (denominator == 0):
            if ((bigcircle["y"] - smallcircle["y"]) < 0):
                angle = -90
            else:
                angle = 90
            heading = angle
        else:
            correctRobotPositionPerspective(bigcircle, originalWarpedFrame, smallcircle)
            angle = np.arctan2((bigcircle["y"] - smallcircle["y"]), (bigcircle["x"] - smallcircle["x"]))
            heading = round(angle * 180 / np.pi, 2) #Converts from radians to -180 to 180 degrees
        cv2.putText(originalWarpedFrame, f"Heading: {heading}",
                    (25, originalWarpedFrame.shape[0] - 25),
                    cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1)
        #If we want to show the "fake" position of the robot on screen as the camera sees it
        if showrobotcameraposition:
            cv2.line(originalWarpedFrame, (bigcircle["x"], bigcircle["y"]), (smallcircle["x"], smallcircle["y"]),
                     (255, 0, 0), 3)
            cv2.arrowedLine(originalWarpedFrame, (smallcircle["x"], smallcircle["y"]), (bigcircle["x"], bigcircle["y"]),
                            (255, 0, 0), 1)
    else:
        print(f"Amount of orientation points: {len(orientationpoints)}")
    return bigcircle, smallcircle, heading

#Find the robot circles and fill it into orientation points
def findRobotCircles(orientationpoints, originalWarpedFrame, showrobotcameraposition):
    framelab = cv2.cvtColor(originalWarpedFrame,cv2.COLOR_BGR2LAB)  # Convert to LAB
    l, a, b = cv2.split(framelab)
    # Apply a treshhold, remove everything that is not green
    ret, threshrobotgreen = cv2.threshold(a, cv2.getTrackbarPos('thresh-lwr-robot-green', 'robot'),
                                          cv2.getTrackbarPos('thresh-upr-robot-green', 'robot'),
                                          cv2.THRESH_BINARY_INV)

    cv2.imshow("Threshold robot circles", threshrobotgreen)
    #Find the contours of the circles
    contours, hierarchy = cv2.findContours(threshrobotgreen, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        M = cv2.moments(c) #Finds the center of a "mass"/"contour"
        if (M["m00"] != 0.0 and cv2.contourArea(c) >= 400):
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            orientationpoints.append(({"x": cX, "y": cY, "area": cv2.contourArea(c)}))
            #print(cv2.contourArea(c))
            if (showrobotcameraposition):
                cv2.circle(originalWarpedFrame, (cX, cY), 2, (0, 0, 255), -1)
                cv2.putText(originalWarpedFrame, f"Robot: {cX},{cY}", (cX - 25, cY - 25),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 2)
                cv2.fillPoly(originalWarpedFrame, pts=[c], color=(255, 0, 255))

#Detects the cross by finding the contours
def detectCross(edgesDetected, frame):
    cv2.imshow("edgesDetected",edgesDetected)
    cv2.imshow("frame2", frame)
    contours, hierarchy = cv2.findContours(edgesDetected, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cross = np.zeros((frame.shape[0], frame.shape[1]), np.uint8) #Create empty frame
    #Loop over contours and check they fulfilll certain size criterias
    for c in contours:
        if (cv2.arcLength(c, True) < 400 and cv2.arcLength(c, True) > 100 and cv2.contourArea(c) > 1000):
            cv2.fillPoly(cross, pts=[c], color=255) #Fill the empty frame with white polygons
    cv2.imshow("cross",cross)
    #Edge detection function
    crossEdges = cv2.Canny(cross, threshold1=cv2.getTrackbarPos('smallest-edge', 'map'),
                           threshold2=cv2.getTrackbarPos('largest-edge', 'map'))
    #Find lines defining the cross for later use in the svg
    linescross = cv2.HoughLinesP(image=crossEdges, rho=1, theta=np.pi / 180,
                                 threshold=cv2.getTrackbarPos('thresh-cross', 'map'), lines=np.array([]),
                                 minLineLength=cv2.getTrackbarPos('min-line-length-cross', 'map'),
                                 maxLineGap=cv2.getTrackbarPos('max-line-length-cross', 'map'))
    return linescross


# Remove things outside the field and applies some filters which are used in other parts
def cleanupImage(frame, originalGrayFrame):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)  # Convert to LAB
    l, a, b = cv2.split(frame)
    # Applies a filter which is edge preserving and noise reducingcv2.createTrackbar('thresh-lwr-balls', 'balls', 210, 255, nothing)
    #     cv2.createTrackbar('thresh-upr-balls', 'balls', 255, 255, nothing)
    a = cv2.bilateralFilter(src=a, d=cv2.getTrackbarPos('pixel-diameter', 'map'),
                            sigmaColor=cv2.getTrackbarPos('color-closeness', 'map'),
                            sigmaSpace=cv2.getTrackbarPos('pixel-closeness', 'map'), borderType=cv2.BORDER_DEFAULT)
    # Remove certain things in the image
    cv2.imshow("A before",a)
    ret, a = cv2.threshold(a, cv2.getTrackbarPos('size-lwr-thresh', 'map'),
                           cv2.getTrackbarPos('size-uppr-thresh', 'map'),
                           0)
    cv2.imshow("A after", a)
    # Detect edges
    edgesDetected = cv2.Canny(a, threshold1=cv2.getTrackbarPos('smallest-edge', 'map'),
                              threshold2=cv2.getTrackbarPos('largest-edge', 'map'))
    cv2.imshow("edgesd",edgesDetected)
    '''
    mask = edgesDetected.copy()
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for c in contours:
        cv2.fillPoly(mask, pts=[c], color=255)
    # Fill the field with a mask
    ret, mask = cv2.threshold(mask, 220, 255, cv2.THRESH_BINARY)
    # Perform a bitwise and to remove everything outside the field
    cv2.bitwise_and(src1=mask, src2=originalGrayFrame, dst=originalGrayFrame)
    '''
    return a, edgesDetected, frame

#Adjust gamma on a frame
def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)

#Detection of balls
def detectBalls(croppedframe):
    cv2.imshow("grey before",croppedframe)
    #CLAHE performs some normalization on the image to make it more light resistant.
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    cl1 = clahe.apply(croppedframe)
    croppedframe = cl1
    croppedframe = adjust_gamma(croppedframe, 0.6) #Gamma adjustment for same purpose as above
    cv2.imshow("grey after", croppedframe)
    #Remove everything that does not have the right color i.e. white
    ret, croppedframe = cv2.threshold(croppedframe, cv2.getTrackbarPos('thresh-lwr-balls', 'balls'),
                                      cv2.getTrackbarPos('thresh-upr-balls', 'balls'),
                                      cv2.THRESH_BINARY)  # Apply a treshhold
    croppedframe = cv2.morphologyEx(croppedframe, cv2.MORPH_OPEN, (3, 3), iterations=2)  # Remove noise
    contours, hierarchy = cv2.findContours(croppedframe, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) #Find contours
    ballspositions = []
    for c in contours:
        M = cv2.moments(c) #Find center of mass
        #If perimitter length of a contour is within certain treshold we know it is a ball.
        if (cv2.arcLength(c, True) >= cv2.getTrackbarPos('balls-arc-lwr', 'balls') and cv2.arcLength(c,
                                                                                                     True) <= cv2.getTrackbarPos(
                'balls-arc-upr', 'balls')):  #Filter out things with a certain arcLength/perimiter length
            if (M["m00"] != 0.0):
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
                ballspositions.append({"x": cX, "y": cY})
    return croppedframe, ballspositions



def createFileHeader():
    f = open('path.svg', 'w+')
    f.write('<svg width="' + str(imageWidth) + '" height="' + str(imageHeight) + '" xmlns="http://www.w3.org/2000/svg">\n')
    return f
# LINK:https://www.pyimagesearch.com/2014/08/25/4-point-opencv-getperspective-transform-example/
# Sets the frame corners, ONLY used for perspective correction
def setFrameCorners():
    global pts, result, counter, point
    pts = np.zeros((4, 2))
    perspectivePointsLoaded = False
    try:
        pts = np.load("./perspectivepoints.npy")
        if calibrationMode:
            result = input("Would you like to load the last known perspective points? Y/N: ")
            if (result.lower() != "n"):
                perspectivePointsLoaded = True
                print(f"Loaded: {pts}")
            else:
                perspectivePointsLoaded = False
        else:
            perspectivePointsLoaded = True
    except:
        print("Error while loading perspective points")
        print(pts)
        perspectivePointsLoaded = False
    if not perspectivePointsLoaded:
        pts = np.zeros((4, 2))
        counter = 0
        points = perspective.readpoints(cap)
        counter = 0
        for point in points:
            pts.put(counter, point["x"])
            counter += 1
            pts.put(counter, point["y"])
            counter += 1
        np.save("./perspectivepoints", pts)

        print(f"Saving perspective points {points} ")
#Mark the area the robot can move within
def markFieldLines():
    global fieldpoints, lines, f, result, ret, frame, originalPerspectiveCorrectedFrame, point, i
    fieldpoints = []
    lines = []
    boundaryLinesLoaded = False
    boundaryLinesLoaded = loadFieldData(boundaryLinesLoaded)
    #Define field dimensions
    while True and not boundaryLinesLoaded:
        ret, frame = cap.read()
        cv2.imshow("Original feed", frame)
        #Load pts from global scope for perspective correction
        originalPerspectiveCorrectedFrame = perspective.four_point_transform(frame, pts)
        originalPerspectiveCorrectedFrame = perspective.four_point_transform(frame, pts)
        cv2.imshow("Perspective warped feed", originalPerspectiveCorrectedFrame)

        cv2.namedWindow("Warped field calibration")
        cv2.setMouseCallback("Warped field calibration", mouse_drawing_field)
        if (len(fieldpoints) != 0):
            for point in fieldpoints:
                cv2.circle(originalPerspectiveCorrectedFrame, (int(point["x"]), int(point["y"])), 1, (0, 255, 255), -1)
        #When we have 4 points marked we create the lines defining the field
        if (len(fieldpoints) == 4):
            for i in range(4):
                if (i != 3):
                    lines.append(({"x1": fieldpoints[i]["x"], "y1": fieldpoints[i]["y"], "x2": fieldpoints[i + 1]["x"],
                                   "y2": fieldpoints[i + 1]["y"]}))
                else:
                    lines.append(({"x1": fieldpoints[i]["x"], "y1": fieldpoints[i]["y"], "x2": fieldpoints[0]["x"],
                                   "y2": fieldpoints[0]["y"]}))
            jsonWriter = json.dumps(lines)
            with open("lines.json", "w") as f:
                f.write(jsonWriter)

            print(f"JSON-fieldpoints: {fieldpoints}\t type:{type(fieldpoints)}")
            jsonWriter2 = json.dumps(fieldpoints)
            with open("fieldpoints.json", "w") as f:
                f.write(jsonWriter2)
            print(f"Saving line points: {lines}")
            break
        cv2.imshow("Warped field calibration", originalPerspectiveCorrectedFrame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def loadFieldData(boundaryLinesLoaded):
    global f, lines, result, fieldpoints
    try:
        with open("lines.json", "r") as f:
            lines = json.load(f)
        if calibrationMode:
            result = input("Would you like to load the last known boundary lines? Y/N: ")
            if (result.lower() != "n"):
                boundaryLinesLoaded = True
                print(f"Loaded lines: {lines}")
                with open("fieldpoints.json", "r") as f:
                    fieldpoints = json.load(f)
            else:
                boundaryLinesLoaded = False
                lines = []
        else:
            boundaryLinesLoaded = True
            with open("fieldpoints.json", "r") as f:
                fieldpoints = json.load(f)
    except:
        print("Error while loading lines from file")
        boundaryLinesLoaded = False
    return boundaryLinesLoaded

#Find MaxX,MaxY,MinX,MinY of the field
def findInnerBorder():
    global innerBorderMaxY, innerBorderMinY, innerBorderMaxX, innerBorderMinX
    innerBorderMaxY = 0
    innerBorderMinY = 1000
    innerBorderMaxX = 0
    innerBorderMinX = 1000
    for coord in lines:
        if coord['x1'] > innerBorderMaxX:
            innerBorderMaxX = coord['x1']
        if coord['x1'] < innerBorderMinX:
            innerBorderMinX = coord['x1']
        if coord['y1'] > innerBorderMaxY:
            innerBorderMaxY = coord['y1']
        if coord['y1'] < innerBorderMinY:
            innerBorderMinY = coord['y1']
    innerBorderCords = {'MaxX': innerBorderMaxX,
                        'MaxY': innerBorderMaxY,
                        'MinX': innerBorderMinX,
                        'MinY': innerBorderMinY}



def generateSVG():
    global lines, originalPerspectiveCorrectedFrame
    #global ret, frame, originalPerspectiveCorrectedFrame, lines, linescross, scalingfactor, ballspositions, maxline, robotPosition, heading, crossMid, line, ball, f
    counter = 0
    f = createFileHeader()

    while True:
        ret, frame = cap.read()
        #Move the perspective to birds eye view
        originalPerspectiveCorrectedFrame = perspective.four_point_transform(frame, pts)
        #Do object recognition
        lines, linescross, scalingfactor, ballspositions, maxline, robotPosition, heading, crossMid = objectdetection(
            originalPerspectiveCorrectedFrame, lines)
        linescross = []
        crossMid["x"] = crossMid["x"]*scalingfactor
        crossMid["y"] = crossMid["y"] * scalingfactor
        trackWidth = (14/2)+2
        linescross.append({"x1":crossMid["x"] + 10 + trackWidth,"y1":crossMid["y"]-10-trackWidth,"x2":crossMid["x"]+10+trackWidth,"y2":crossMid["y"]+10+trackWidth})
        linescross.append({"x1": crossMid["x"] + 10 + trackWidth, "y1": crossMid["y"] + 10+trackWidth, "x2": crossMid["x"] - 10-trackWidth, "y2": crossMid["y"] + 10+trackWidth})
        linescross.append({"x1": crossMid["x"] - 10 - trackWidth, "y1": crossMid["y"] + 10+trackWidth, "x2": crossMid["x"] - 10-trackWidth, "y2": crossMid["y"] - 10-trackWidth})
        linescross.append({"x1": crossMid["x"] - 10 - trackWidth, "y1": crossMid["y"] - 10-trackWidth, "x2": crossMid["x"] + 10+trackWidth, "y2": crossMid["y"] -10-trackWidth})

        for line in lines:
            cv2.line(originalPerspectiveCorrectedFrame, (line["x1"], line["y1"]),
                     (line["x2"], line["y2"]), (255, 255, 255), 1)
        #Writes cross to SVG
        if counter == 1:
            if linescross is not None:
                # f.write("<!-- Cross below -->\n")
                for line2 in linescross:
                    #for x1, y1, x2, y2 in line2:
                    f.write(
                        '<line x1="' + str(round(line2["x1"], 3)) + '" y1="' + str(
                            round(line2["y1"], 3)) + '" x2="' + str(
                            round(line2["x2"], 3)) + '" y2="' + str(round(
                            line2["y2"], 2)) + '" />\n')
            f.write('</svg>')
            f.close()
            break

        else:
            counter = counter + 1
            #Writes the boundary lines to SVG
            if lines is not None:
                for line in lines:
                    f.write('<line x1="' + str(round(line["x1"] * scalingfactor, 2)) + '" y1="' + str(round(
                        line["y1"] * scalingfactor, 2)) + '" x2="' + str(
                        round(line["x2"] * scalingfactor, 2)) + '" y2="' + str(round(
                        line["y2"] * scalingfactor, 2)) + '" />\n')
                    # style="fill:none;stroke:#000000;stroke-width:1"



def sendAndGenerateSVG():
    global resend_positional_stuff
    resend_positional_stuff = True
    print("Generating new SVG")
    generateSVG()
    print("Finished generating SVG")
    if networkMode:
        socketClient.send_test_svg(client)
    print("SENT SVG")

#Main body starts here..
cap = cv2.VideoCapture(2)
initializeSliders()
setFrameCorners()
markFieldLines()

counter = 0
finished = False
print(lines)
findInnerBorder()

#generateSVG()
#exit(1)

if networkMode:
    client = socketClient.Client()
    time.sleep(2)
    sendAndGenerateSVG()

oldCrossPosition = {}
ballblacklist = np.zeros((180,180,1))
firstTime = True
while True:
    ret, frame = cap.read()
    cv2.imshow("Original feed", frame)
    originalPerspectiveCorrectedFrame = perspective.four_point_transform(frame, pts)

    cv2.imshow("Warped field calibration", originalPerspectiveCorrectedFrame)
    lines, linescross, scalingfactor, ballspositions, maxline, robotPosition, heading, crossMid = objectdetection(
        originalPerspectiveCorrectedFrame, lines)

    for line in lines:
        cv2.line(originalPerspectiveCorrectedFrame, (line["x1"], line["y1"]),
                 (line["x2"], line["y2"]), (255, 255, 255), 1)
    #Normalize object coordinates
    if ballspositions:
        for ball in ballspositions:
            ball["x"] = round((ball["x"]-fieldpoints[0]["x"]) * scalingfactor, 2)
            ball["y"] = round((ball["y"]-fieldpoints[0]["y"]) * scalingfactor, 2)

        #print(f"ballpositions corrected: {ballspositions}")

    if robotPosition:
        robotPosition["x"] = (robotPosition["x"]-fieldpoints[0]["x"])*scalingfactor
        robotPosition["y"] = (robotPosition["y"]-fieldpoints[0]["y"])*scalingfactor
        #print(f"Robotpossition corrected: {robotPosition}")
    if crossMid:
        crossMid["x"] = round((crossMid["x"] - fieldpoints[0]["x"]) * scalingfactor, 2)
        crossMid["y"] = round((crossMid["y"] - fieldpoints[0]["y"]) * scalingfactor, 2)
        #print(f"CROSSMID:{crossMid}")
        #Check if cross has moved since last frame and if so send and generate new svg
        if oldCrossPosition and robotPosition:
            diffX = abs(crossMid["x"] - oldCrossPosition["x"])
            diffY = abs(crossMid["y"] - oldCrossPosition["y"])
            distanceToCrossFromRobot = round(calculateDistanceBetweenTwoPoints(crossMid["x"],robotPosition["x"],crossMid["y"],robotPosition["y"]))
            #print(f"Distance to cross from robot: {distanceToCrossFromRobot}")
            if(diffX >= 2 or diffY>=2) and distanceToCrossFromRobot >= 29:
                crossMoving = True
                oldCrossPosition = crossMid
                print("Cross moving")
            else:
                if crossMoving:
                    print("Cross stopped moving")
                    sendAndGenerateSVG()
                    crossMoving = False
        else:
            oldCrossPosition = crossMid

    goalPoint = {'x': innerBorderMaxX, 'y': findMidOfScreen(originalPerspectiveCorrectedFrame)[0]["y"]}
    cv2.circle(originalPerspectiveCorrectedFrame, (round(goalPoint["x"]), round(goalPoint["y"])), 1, (255, 0, 0), 3)

    if resend_positional_stuff and networkMode:
        if crossMid:
            #print(f"crossmid corrected: {crossMid}")
            socketClient.send_cross_positions(client, crossMid)
            print(f"SENT CROSS!!{crossMid}")
        else:
            socketClient.send_cross_positions(client, [])


        goalPoint["x"] = round((goalPoint["x"] - fieldpoints[0]["x"]) * scalingfactor, 2) - 23
        goalPoint["y"] = round((goalPoint["y"] - fieldpoints[0]["y"]) * scalingfactor, 2)
        socketClient.send_goal_positions(client, goalPoint)

        print(f"Sent goal position!!\t {goalPoint}")
        resend_positional_stuff = False
    cv2.imshow("Perspective warped feed", originalPerspectiveCorrectedFrame)

    #print(f"Heading {heading}")
    for i in range(2):
        if networkMode:
            try:
                message = socketClient.receive_data(client)
            except:
                sendAndGenerateSVG()
        else:
            message = "BALL"
        #Robot ask for ball
        if message == "BALL":
            if len(ballspositions) != 0:
                #Send ball closest to robot
                if len(robotPosition) <= 1:
                    randomBall = ballspositions[random.randint(0,len(ballspositions)-1)]
                    if networkMode:
                        try:
                            socketClient.send_balls_positions(client, randomBall)
                        except:
                            sendAndGenerateSVG()
                    #print(f"SENT BALL {randomBall}")
                else:
                    foundValidBall = False
                    while True:
                        minball = ballspositions[0]
                        if ballblacklist[round(minball["x"]),round(minball["y"]),0] < maxTriesForBall:
                            foundValidBall = True
                        smallestdist = calculateDistanceBetweenTwoPoints(robotPosition["x"],ballspositions[0]["x"],robotPosition["y"],ballspositions[0]["y"])
                        for ball in ballspositions:
                            distToCurrentBall = calculateDistanceBetweenTwoPoints(robotPosition["x"],ball["x"],robotPosition["y"],ball["y"])
                            triesForCurrentBall = ballblacklist[round(ball["x"]),round(ball["y"]),0]
                            if distToCurrentBall < smallestdist and triesForCurrentBall < maxTriesForBall:
                                minball = ball
                                foundValidBall = True
                        if not foundValidBall:
                            try:
                                socketClient.send_balls_positions(client, [])
                                print("NO MORE VALID BALLS")
                                break
                            except:
                                sendAndGenerateSVG()
                        if networkMode:
                            try:
                                print(f"SENT BALL {minball}")
                                ballblacklist[round(minball["x"]),round(minball["y"]),0] = ballblacklist[round(minball["x"]),round(minball["y"]),0]+1
                                if ballblacklist[round(minball["x"]),round(minball["y"]),0] >= maxTriesForBall:
                                    print(f"BLACKLISTED BALL: {minball}")
                                socketClient.send_balls_positions(client, minball)

                                break
                            except:
                                sendAndGenerateSVG()
                        else:
                            break

            else:
                if networkMode:
                    try:
                        if firstTime:
                            goalPoint["x"] = round((goalPoint["x"] - fieldpoints[0]["x"]) * scalingfactor, 2) - 30
                            goalPoint["y"] = round((goalPoint["y"] - fieldpoints[0]["y"]) * scalingfactor, 2)
                            print(f"Sending fake ball {goalPoint}")
                            socketClient.send_balls_positions(client, goalPoint)
                            firstTime = False
                        else:
                            socketClient.send_balls_positions(client,[])
                            firstTime = True
                    except:
                        sendAndGenerateSVG()
        #Robot asks for pose
        elif message == "POSE":
            robotPosition["heading"] = heading
            print(f"RECIEVED MESSAGE TO SEND POSE, CUR POSE: ", robotPosition)
            if networkMode:
                if len(robotPosition) == 4:
                    try:
                        socketClient.send_robot_positions(client, robotPosition)
                    except:
                        sendAndGenerateSVG()
                else:
                    try:
                        socketClient.send_robot_positions(client, {})
                        print(f"SENT EMPTY POSE")
                    except:
                        sendAndGenerateSVG()
        elif message == "ERROR":
            print("ERROR HAPPEND WHILE RECEIVING DATA")
        elif message == "NODATA":
            continue
