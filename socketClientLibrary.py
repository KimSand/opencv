import socket
import re

from time import sleep



class Client:
    """
    Class responsible for the socket connection.
    Data is written as UTF-8 and are sent bitwise.
    """
    client_socket_receiver = None
    client_socket_sender = None
    host="192.168.1.122"
    port = [1234, 1235]
    connected = False
    remote_ip = ""
    def __init__(self):
        """
        Creates and connects a socket.
        Default host is localhost and default port is 1234.

        Documentation for the socket creation:
        AF_INET: https://docs.python.org/3/library/socket.html#socket.AF_INET
        """
        if len(self.port) != 2:
            raise Exception("Length of port is not 2")
        # Checking whether the host is a ipv4 address or hostname:
        regex = re.compile("^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$")  # RegExpr for IPv4 address
        if regex.search(self.host):
            print("Host is an IP address...")
            self.remote_ip = self.host
        else:
            print("Getting remote ip...")
            self.remote_ip = None
            try:
                self.remote_ip = socket.gethostbyname(self.host)
            except socket.gaierror as e:
                print(str(e))
                exit(-1)

        print("Connecting to servers")
        self.connect()
        print("Servers connected!")

    def connect(self):

        while not self.connected:
            try:
                print("Trying to establish sockets")
                self.close()
                self.client_socket_sender = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.client_socket_receiver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.client_socket_receiver.settimeout(0.1)
                print("Creating sender socket")
                self.client_socket_sender.connect((self.remote_ip, self.port[1]))
                print("Sender socket created sucessfully")
                sleep(1)
                print("Creating receiver socket")
                self.client_socket_receiver.connect((self.remote_ip, self.port[0]))
                print("Receiver socket created sucessfully")
                self.connected = True
            except Exception as e:
                print("Error while establihshing connection")
                self.connected = False
                sleep(1)



    def receive_data(self):
        """
        This receive data from the socket
        :return: Byte-like object received
        """

        try:
            self.client_socket_receiver.send("aa".encode("utf-8"))
            self.client_socket_receiver.sendall("aa".encode("utf-8"))
            self.client_socket_receiver.sendall("aa".encode("utf-8"))
            data =  self.client_socket_receiver.recv(6)
            return data
        except socket.timeout as e:
            raise e
        except Exception as e:
            self.connected = False
            print("Real exception while receiving, trying to reconnect")
            self.connect()
            sleep(1)
            raise Exception



    def send_data(self, data):
        """
        This sends data through the socket
        :param data: String or byte to be sent
        :return: None
        """
        try:
            string = data + "\r\n"
            self.client_socket_sender.sendall(string.encode("utf-8"))
        except Exception as e:
            print(f"Error occurred while sending {str(e)}")
            self.connected = False
            self.connect()
            print("Now trying to resend data")
            sleep(1)
            raise e

    def close(self):
        """
        This closes the socket connection
        :return: None
        """
        if self.client_socket_sender is not None:
            self.client_socket_sender.close()
        if self.client_socket_receiver is not None:
            self.client_socket_receiver.close()


def main():
    """
    This is a example of use of the above class
    :return: None
    """
    client = Client()
    print("Give a input:")

    print(client.receive_data())
    while True:
        input_line = input("> ")
        if not input_line:
            break
        client.send_data(input_line)

    client.send_data("exit")
    print("Connection destroyed!")
    client.close()


if __name__ == "__main__":
    main()
